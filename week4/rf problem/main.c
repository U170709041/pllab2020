#include <stdio.h>
#include <stdlib.h>
#include <math.h>



double arb_func(float x){

return x*log10(x)-1.2;
}

void rf(float *x,float x1,float x2,float fx0, float fx1,int *itr,FILE*outp){

    *x = ((x1 * fx1) - (x2 * fx0))/(fx1 - fx0);
    ++*itr;
    printf(" iteration %d: %.5f\n", *itr, *x);
    fprintf(outp," iteration %d: %.5f\n", *itr, *x);

}


int main(){

int itr;
int maxitr;
float x1,x2,x_curr,x_next,error;

FILE *outp;

outp = fopen("rf.txt", "w");

printf("enter interval values  [x1,x2], allowed error and number of iterations:");
scanf("%f%f%f%d", &x1,&x2,&error,&maxitr);

rf(&x_curr,x1,x2,arb_func(x1),arb_func(x2),&itr,outp);

do{
    if ((arb_func(x1) * arb_func(x_curr)) < 0){

        x2=x_curr;

    }else{
    x1 = x_curr;
    }

rf(&x_next,x1,x2,arb_func(x1),arb_func(x2),&itr,outp);

if(fabs(x_next - x_curr) > error){
    printf("after %d iterations. root is %.5f\n",itr,x_next);
    fprintf(outp,"after %d iterations. root is %.5f",itr,x_next);

    return 0;

}else {
    x_next=x_curr;}
}while(itr < maxitr);

printf("solution does not converge or iterations not sufficient");
fprintf(outp,"solution does not converge or iterations not sufficient");

fclose(outp);

return 1;


}
